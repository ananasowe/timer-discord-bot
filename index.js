const Discord = require('discord.js');
const client = new Discord.Client();

const farewells = [
	"Finish line!",
	"Finish!",
	"Crossed the finish line!",
	"Swept through the finish line!",
	"Amazing race! We've reached the finish!",
	"Finish line passed!",
	"Went through the finish line!",
	"Nose behind the finish line!",
	"Race finished!"
];

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
	client.user.setActivity("Mjeri vreme", { type: 'WATCHING'});
});

client.on('message', msg => {
  if (msg.content === '!tk ping') {
    msg.reply('Yes. I\'m alive');
  }
});

client.on("guildMemberRemove", member => {
	try {
		let diffMs = new Date().getTime() - member.joinedAt.getTime();
		let milis = diffMs;
		let seconds = Math.floor(milis / 1000);
		milis = milis % 1000;
		let minutes = Math.floor(seconds / 60);
		seconds = seconds % 60;

		if (minutes < 60 && member.guild.systemChannel != null) {
			(async () => {
				let time = `⏱️ User ${member.displayName} left the server in `;
				if (minutes > 0) {
					time = time.concat(`**${minutes}**`);
					if (minutes > 1) time = time.concat(' minutes, '); else time = time.concat(' minute, ');
				}
				time = time.concat(`**${seconds}**`);
				if (seconds > 1) time = time.concat(' seconds and '); else time = time.concat(' second and ');
				time = time.concat(`**${milis}**`);
				if (seconds > 1) time = time.concat(' milliseconds!'); else time = time.concat(' millisecond!');

				if(minutes < 1 && seconds < 5) time = time.concat(" This is remarkable!");
				else if(minutes < 1 && seconds < 10) time = time.concat(" What an incredible speed!");

				let fetchedLogs = await member.guild.fetchAuditLogs({
					limit: 1,
					type: 'MEMBER_KICK',
				});
				let kicked = (fetchedLogs.entries.first().target.id == member.user.id) && (fetchedLogs.entries.first().createdAt > member.joinedAt);
				fetchedLogs = await member.guild.fetchAuditLogs({
					limit: 1,
					type: 'MEMBER_BAN_ADD',
				});
				let banned = (fetchedLogs.entries.first().target.id == member.user.id) && (fetchedLogs.entries.first().createdAt > member.joinedAt);
				console.log(`User ${member.user.tag} left the server. Kicked? ${kicked} Banned? ${banned}`);

				let finishMsg;
				if(kicked) {
					finishMsg = "🚫 Disqualification";
					time = time.concat("\nDue to ungentlemanly behavior, user got removed from the race by a server admin.");
				} else if (banned) {
					finishMsg = "☠️ Ban hammer has spoken";
					time = "User got banned from the Olympics";
					let reason = fetchedLogs.entries.first().reason;
					if(!reason.startsWith("~")) {
						time = time.concat(`\n(*${reason}*)`);
					}
				} else {
					finishMsg = "🏁 ".concat(farewells[Math.floor(Math.random() * farewells.length)]);
				}

				let leaveEmbed = new Discord.MessageEmbed()
					.setAuthor(member.user.tag, member.user.displayAvatarURL())
					.setColor('#000')
					.setTitle(finishMsg)
					.setDescription(time);
				await member.guild.systemChannel.send(leaveEmbed);
			})();
		}
	} catch (exception) {
		console.error(exception);
	}
});

client.on("guildBanAdd", (guild, user) => {
	console.log(`User ${user.tag} got banned`);
});

client.login('NzQzOTUxNjgwMTU1OTQzMDE0.XzcI_g.txZxbJpDi8yDkWy6LzplTVO_2Vg');
